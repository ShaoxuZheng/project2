Computed Water Depth (h)
Time h_0 h_1 h_2 h_3 h_4 h_5 h_6 h_7 h_8 h_9 h_10 h_11 h_12 h_13 h_14 h_15 h_16 h_17 h_18 h_19 h_20 
   0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0 
   1  3.1  3.1  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.1  3.1 
   2  3.2  3.1  3.1  3.1  3.1  3.1  3.1  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.1  3.1  3.1  3.2  3.2 
   3  3.2  3.2  3.2  3.2  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.2  3.2  3.2  3.2  3.3 
   4  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.3  3.3  3.3  3.4 
   5  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.3  3.3  3.3  3.3  3.3  3.3  3.4  3.4  3.4  3.5 
   6  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.4  3.4  3.4  3.4  3.5  3.5  3.5  3.6 
   7  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.5  3.5  3.5  3.5  3.6  3.6  3.6 
   8  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.5  3.5  3.5  3.5  3.5  3.5  3.6  3.6  3.6  3.7  3.7  3.7 
   9  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.6  3.6  3.6  3.6  3.6  3.7  3.7  3.7  3.8  3.8  3.8 
  10  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.7  3.7  3.7  3.7  3.8  3.8  3.8  3.9  3.9  3.9 
  11  3.6  3.6  3.6  3.6  3.6  3.6  3.7  3.7  3.7  3.7  3.7  3.7  3.8  3.8  3.8  3.8  3.9  3.9  3.9  4.0  4.0 
  12  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.8  3.8  3.8  3.8  3.8  3.9  3.9  3.9  4.0  4.0  4.0  4.1  4.1 
  13  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.9  3.9  3.9  3.9  4.0  4.0  4.0  4.1  4.1  4.1  4.2  4.2 
  14  3.8  3.8  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  4.0  4.0  4.0  4.1  4.1  4.1  4.1  4.2  4.2  4.3  4.3 
  15  3.9  3.9  3.9  3.9  3.9  4.0  4.0  4.0  4.0  4.0  4.1  4.1  4.1  4.1  4.2  4.2  4.2  4.3  4.3  4.3  4.4 
  16  4.0  4.0  4.0  4.0  4.0  4.0  4.1  4.1  4.1  4.1  4.1  4.2  4.2  4.2  4.3  4.3  4.3  4.4  4.4  4.4  4.5 
  17  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.2  4.2  4.2  4.2  4.3  4.3  4.3  4.3  4.4  4.4  4.5  4.5  4.5  4.6 
  18  4.1  4.1  4.2  4.2  4.2  4.2  4.2  4.2  4.3  4.3  4.3  4.3  4.4  4.4  4.4  4.5  4.5  4.5  4.6  4.6  4.7 
  19  4.2  4.2  4.2  4.2  4.3  4.3  4.3  4.3  4.3  4.4  4.4  4.4  4.5  4.5  4.5  4.6  4.6  4.6  4.7  4.7  4.8 
  20  4.3  4.3  4.3  4.3  4.3  4.4  4.4  4.4  4.4  4.5  4.5  4.5  4.6  4.6  4.6  4.7  4.7  4.7  4.8  4.8  4.9 
  21  4.4  4.4  4.4  4.4  4.4  4.4  4.5  4.5  4.5  4.5  4.6  4.6  4.6  4.7  4.7  4.7  4.8  4.8  4.9  4.9  4.9 
  22  4.4  4.5  4.5  4.5  4.5  4.5  4.6  4.6  4.6  4.6  4.7  4.7  4.7  4.8  4.8  4.8  4.9  4.9  4.9  4.9  5.0 
  23  4.5  4.5  4.5  4.6  4.6  4.6  4.6  4.7  4.7  4.7  4.7  4.7  4.8  4.8  4.8  4.8  4.8  4.9  4.9  4.9  4.9 
  24  4.5  4.6  4.6  4.6  4.6  4.6  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.8  4.8  4.8  4.8  4.8  4.8  4.8  4.8 
  25  4.6  4.6  4.6  4.6  4.6  4.6  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7 
  26  4.5  4.5  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6 
  27  4.4  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5 
  28  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4 
  29  4.3  4.3  4.3  4.3  4.3  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3 
  30  4.2  4.2  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2 
  31  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1 
  32  4.1  4.1  4.1  4.1  4.1  4.1  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.0 
  33  4.0  4.0  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  3.9 
  34  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9 
  35  3.9  3.9  3.9  3.9  3.9  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8 
  36  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.7  3.7  3.7 
  37  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.6  3.6 
  38  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.5  3.5 
  39  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.5  3.5  3.4  3.4 
  40  3.6  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.4  3.4  3.3 
  41  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.4  3.4  3.3  3.3  3.2 
  42  3.5  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.3  3.3  3.3  3.2  3.1 
  43  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.2  3.2  3.1  3.0 
  44  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.2  3.2  3.1  3.1  3.0 
  45  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.1  3.1  3.1 
  46  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.2  3.1  3.1 
  47  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.2  3.2  3.1 
  48  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.2  3.2  3.2 
  49  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.2 
  50  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2 
  51  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3 
  52  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.3  3.3 
  53  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3 
  54  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4 
  55  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.4  3.4  3.4 
  56  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4 
  57  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5 
  58  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.5  3.5 
  59  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5 
  60  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6 
  61  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6 
  62  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7 
  63  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7 
  64  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7 
  65  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8 
  66  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.8  3.8 
  67  4.1  4.1  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8 
  68  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9 
  69  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9 
  70  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9 
  71  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0 
  72  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0 
  73  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0 
  74  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1 
  75  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1 
  76  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1 
  77  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.2  4.2  4.2 
  78  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2 
  79  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2 
  80  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3 
  81  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3 
  82  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3 
  83  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4 
  84  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4 
  85  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5 
  86  4.8  4.8  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.5 
  87  4.8  4.8  4.8  4.8  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5 
  88  4.8  4.8  4.8  4.8  4.8  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.4  4.4 
  89  4.8  4.8  4.8  4.8  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.4  4.4  4.3 
  90  4.8  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.4  4.4  4.4  4.3  4.2 
  91  4.7  4.7  4.7  4.7  4.7  4.7  4.7  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.4  4.4  4.3  4.3  4.2  4.2 
  92  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.3  4.3  4.2  4.2  4.1 
  93  4.6  4.6  4.6  4.6  4.6  4.6  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.3  4.3  4.2  4.2  4.1  4.0 
  94  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.2  4.2  4.1  4.0  4.0 
  95  4.5  4.5  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.2  4.2  4.1  4.1  4.0  4.0  3.9 
  96  4.4  4.4  4.4  4.4  4.4  4.4  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.1  4.1  4.0  4.0  3.9  3.8 
  97  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.3  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.0  4.0  3.9  3.8  3.8 
  98  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.0  4.0  3.9  3.9  3.8  3.8  3.7 
  99  4.2  4.2  4.2  4.2  4.2  4.2  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  3.9  3.9  3.8  3.8  3.7  3.6 
 100  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.1  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.8  3.8  3.7  3.6  3.6 
 101  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.8  3.8  3.7  3.7  3.6  3.6  3.5 
 102  4.0  4.0  4.0  4.0  4.0  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.7  3.7  3.6  3.6  3.5  3.4 
 103  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.9  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.6  3.6  3.5  3.4  3.3 
 104  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.8  3.7  3.7  3.7  3.7  3.6  3.6  3.5  3.5  3.4  3.4  3.3 
 105  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.5  3.5  3.4  3.4  3.3  3.2 
 106  3.7  3.7  3.7  3.7  3.7  3.7  3.7  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.4  3.4  3.3  3.2  3.1 
 107  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.6  3.5  3.5  3.5  3.5  3.4  3.4  3.3  3.3  3.2  3.2  3.1 
 108  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.5  3.4  3.4  3.4  3.4  3.3  3.3  3.2  3.2  3.1  3.0 
 109  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.2  3.2  3.1  3.0  2.9 
 110  3.3  3.3  3.4  3.4  3.4  3.4  3.4  3.4  3.4  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.1  3.1  3.0  3.0  2.9 
 111  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.3  3.2  3.2  3.2  3.2  3.1  3.1  3.0  3.0  2.9  2.8 
 112  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.2  3.1  3.1  3.1  3.1  3.0  3.0  2.9  2.8  2.7 
 113  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.1  3.0  3.0  2.9  2.9  2.8  2.7  2.7 
 114  3.0  3.0  3.0  3.0  3.1  3.1  3.1  3.1  3.1  3.1  3.0  3.0  3.0  3.0  3.0  2.9  2.9  2.8  2.8  2.7  2.6 
 115  2.9  2.9  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  2.9  2.9  2.9  2.9  2.8  2.8  2.7  2.6  2.5 
 116  2.8  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.8  2.8  2.8  2.7  2.7  2.6  2.5  2.4 
 117  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.7  2.7  2.7  2.6  2.6  2.5  2.4 
 118  2.7  2.7  2.7  2.7  2.7  2.7  2.8  2.8  2.8  2.8  2.7  2.7  2.7  2.7  2.7  2.6  2.6  2.6  2.5  2.4  2.3 
 119  2.6  2.6  2.6  2.6  2.6  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.6  2.6  2.6  2.5  2.5  2.4  2.3  2.2 
 120  2.5  2.5  2.5  2.5  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.5  2.5  2.5  2.4  2.4  2.3  2.2 
 121  2.4  2.4  2.4  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.4  2.4  2.3  2.3  2.2  2.1 
 122  2.3  2.3  2.3  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.3  2.3  2.2  2.1  2.0 
 123  2.2  2.2  2.3  2.3  2.3  2.3  2.3  2.4  2.4  2.4  2.4  2.4  2.4  2.3  2.3  2.3  2.3  2.2  2.2  2.1  2.0 
 124  2.1  2.1  2.2  2.2  2.2  2.2  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.2  2.2  2.1  2.1  2.0  1.9 
 125  2.0  2.0  2.1  2.1  2.1  2.1  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.1  2.1  2.0  1.9  1.8 
 126  1.9  1.9  2.0  2.0  2.0  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.0  1.9  1.9  1.8 
 127  1.8  1.8  1.9  1.9  1.9  2.0  2.0  2.0  2.0  2.0  2.1  2.1  2.1  2.0  2.0  2.0  2.0  1.9  1.9  1.8  1.7 
 128  1.7  1.7  1.7  1.8  1.8  1.9  1.9  1.9  1.9  2.0  2.0  2.0  2.0  2.0  2.0  1.9  1.9  1.9  1.8  1.7  1.6 
 129  1.5  1.6  1.6  1.7  1.7  1.8  1.8  1.8  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.8  1.8  1.7  1.7  1.5 
 130  1.4  1.5  1.5  1.6  1.6  1.7  1.7  1.7  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.7  1.7  1.6  1.5 
 131  1.4  1.4  1.5  1.5  1.5  1.6  1.6  1.6  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.6  1.6  1.5 
 132  1.3  1.4  1.4  1.4  1.5  1.5  1.5  1.6  1.6  1.6  1.6  1.7  1.7  1.7  1.7  1.7  1.7  1.6  1.6  1.6  1.6 
 133  1.3  1.3  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6 
 134  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.7 
 135  1.3  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.6  1.6  1.7  1.7 
 136  1.3  1.3  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.6  1.7  1.7 
 137  1.3  1.3  1.3  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.7  1.7  1.8 
 138  1.3  1.3  1.3  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.6  1.6  1.7  1.7  1.7  1.8 
 139  1.3  1.3  1.3  1.3  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.6  1.6  1.6  1.7  1.7  1.8  1.8 
 140  1.4  1.4  1.3  1.3  1.4  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.6  1.6  1.7  1.7  1.8  1.8  1.9 
 141  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.6  1.6  1.7  1.7  1.7  1.8  1.8  1.9 
 142  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.4  1.5  1.5  1.5  1.5  1.6  1.6  1.7  1.7  1.7  1.8  1.8  1.9  1.9 
 143  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.7  1.7  1.8  1.8  1.9  1.9  2.0 
 144  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.5  1.6  1.6  1.6  1.7  1.7  1.7  1.8  1.8  1.9  1.9  2.0  2.0 
 145  1.6  1.6  1.6  1.6  1.5  1.5  1.6  1.6  1.6  1.6  1.6  1.7  1.7  1.7  1.8  1.8  1.9  1.9  1.9  2.0  2.0 
 146  1.7  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.6  1.7  1.7  1.7  1.7  1.8  1.8  1.9  1.9  1.9  2.0  2.0  2.1 
 147  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.8  1.8  1.8  1.9  1.9  1.9  2.0  2.0  2.1  2.1 
 148  1.8  1.8  1.7  1.7  1.7  1.7  1.7  1.7  1.7  1.8  1.8  1.8  1.8  1.9  1.9  1.9  2.0  2.0  2.1  2.1  2.1 
 149  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.8  1.9  1.9  1.9  1.9  2.0  2.0  2.0  2.1  2.1  2.2 
 150  1.9  1.9  1.9  1.8  1.8  1.8  1.8  1.8  1.8  1.9  1.9  1.9  1.9  2.0  2.0  2.0  2.1  2.1  2.1  2.2  2.2 
 151  2.0  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.9  1.9  2.0  2.0  2.0  2.1  2.1  2.1  2.2  2.2  2.2 
 152  2.0  2.0  2.0  2.0  2.0  1.9  1.9  1.9  2.0  2.0  2.0  2.0  2.0  2.0  2.1  2.1  2.1  2.2  2.2  2.2  2.3 
 153  2.1  2.1  2.0  2.0  2.0  2.0  2.0  2.0  2.0  2.0  2.0  2.0  2.1  2.1  2.1  2.1  2.2  2.2  2.2  2.3  2.3 
 154  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.2  2.2  2.2  2.2  2.3  2.3  2.3 
 155  2.2  2.2  2.2  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.1  2.2  2.2  2.2  2.2  2.3  2.3  2.3  2.3  2.4 
 156  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.3  2.3  2.3  2.4  2.4  2.4 
 157  2.3  2.3  2.3  2.3  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.2  2.3  2.3  2.3  2.3  2.3  2.4  2.4  2.4  2.5 
 158  2.4  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.4  2.4  2.4  2.4  2.5  2.5 
 159  2.4  2.4  2.4  2.4  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.3  2.4  2.4  2.4  2.4  2.4  2.4  2.5  2.5  2.5 
 160  2.5  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.5  2.5  2.5  2.5  2.6 
 161  2.5  2.5  2.5  2.5  2.5  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.4  2.5  2.5  2.5  2.5  2.5  2.5  2.6  2.6 
 162  2.6  2.6  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.6  2.6  2.6  2.6 
 163  2.6  2.6  2.6  2.6  2.6  2.6  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.5  2.6  2.6  2.6  2.6  2.6  2.6  2.7 
 164  2.7  2.7  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.7  2.7  2.7 
 165  2.7  2.7  2.7  2.7  2.7  2.7  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.6  2.7  2.7  2.7  2.7  2.7  2.7 
 166  2.8  2.8  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.8 
 167  2.8  2.8  2.8  2.8  2.8  2.8  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.7  2.8  2.8  2.8  2.8 
 168  2.9  2.9  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8 
 169  2.9  2.9  2.9  2.9  2.9  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.8  2.9  2.9 
 170  3.0  3.0  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9 
 171  3.0  3.0  3.0  3.0  3.0  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9 
 172  3.1  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  3.0  2.9  2.9  2.9  2.9  2.9  2.9  2.9  2.9  3.0  3.0  3.0 
