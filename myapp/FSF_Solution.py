# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 11:51:23 2018

@author: chaco3
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib import cm

# Get input variables
dt = 5.0  # [s] time step
dx = 20.0  # [m] cell resolution
theta = 0.8
psi = 0.5
l = 500.0  # Length [m]
t = 1.0*3600.0  # [s]  Model run time
b = 10.0  # Width [m]
h0 = 1.0 # [m]
q0 = 0.0  # [m3/s]
hl = 1.0  # [m]
ql = 0.0  # [m3/s]
hr = 1.0  # [h]
qr = 0.0  # [m3/s]
c = 1000000.0  # [-] Chezy
beta = 1.0
g = 9.81
s = 0.000

# Define  type of boundary conditions
lbc_type = 'Q'
rbc_type = 'Q'
max_iter = 15

# Get number of steps (time and distance)
ts = int(t//dt)
xs = int(l//dx)

# Define the value of the left and right boundary conditions
lbc_vec = 3.0*np.ones(ts)
rbc_vec = 1.0*np.ones(ts)

# Initialise the matrix H and Q, t and x
H = np.zeros([ts, xs])
Q = np.zeros([ts, xs])

# Set the initial conditions for Q and H
Q[0, :] = np.interp(np.linspace(0, l, xs), [0, l], [ql, qr])
H[0, :] = np.interp(np.linspace(0, l, xs), [0, l], [hl, hr])

def hd_mod(Q, H, BC=None, bc_type=None):
    '''
    Make a function to run the hydrodynamic model. 
    inputs are matrix Q and H, boundary conditions and type of boundary condition
    
    
    Return 
    Q, H, and courant number
    '''
    
    if BC is None and bc_type is None:
        print('Static test')
        BC = np.zeros([ts, 2])
        bc_type = ['Q', 'Q']
    
    # Parsing inputs
    lbc_vec = BC[:, 0]
    rbc_vec = BC[:, 1]
    lbc_type, rbc_type = bc_type
    
    A1 = -theta/dx
    B1 = b*(1.0 - psi)/dt
    C1 = theta/dx
    D1 = b*psi/dt
    
    q_iter_i = []
    h_iter_i = []
    for ti in xrange(ts-1):
        # make the initial condition in the first iteration
        Q[ti+1, :] = Q[ti, :]
        H[ti+1, :] = H[ti, :]
        for _ in xrange(max_iter):
            
            F = np.zeros([2*(xs), 2*(xs)])
            E = []
            for xj in xrange(xs-1):
                
                E1j = (- ((1.0 - theta)*(Q[ti, xj+1] - Q[ti, xj]) / dx) 
                       + ((1.0 - psi)*b*H[ti, xj] / dt)
                       + (psi*b*H[ti, xj+1] / dt)
                      )
    
                E.append(E1j)
                
                ## momentum equation factors
                hj_nhalf = (H[ti+1, xj] + H[ti, xj]) / 2.0
                hj1_nhalf =(H[ti+1, xj+1] + H[ti, xj+1]) / 2.0
                
                
                Aj_nhalf = b*hj_nhalf
                Aj1_nhalf = b*hj1_nhalf
                Ajhalf_nhalf = (Aj_nhalf + Aj1_nhalf) / 2.0
                
                Kj_nhalf = c*b*(hj_nhalf**1.5)
                Kj1_nhalf = c*b*(hj1_nhalf**1.5)
                
                A2 = (+ ((1.0 - psi)/dt) 
                      - (beta/(Aj_nhalf*dx))*Q[ti, xj] 
                      + (1.0 - psi)*g*Aj_nhalf*(np.abs(Q[ti, xj])/(Kj_nhalf**2))
                      )
                    
                B2 = -g*Ajhalf_nhalf*(theta/dx)
                
                C2 = (+ (psi/dt) 
                      + (beta/(Aj_nhalf*dx))*Q[ti, xj]  
                      + psi*g*Ajhalf_nhalf*(abs(Q[ti, xj+1])/(Kj1_nhalf**2))
                      )
                
                D2 = g*Ajhalf_nhalf*(theta/dx)
                
                E2j = (((1.0 - psi)/dt)*Q[ti, xj] + 
                       (psi/dt)*Q[ti, xj+1] - 
                       g*Ajhalf_nhalf*(1.0 - theta)*(H[ti, xj+1] - H[ti, xj])/dx + 
                       g*Ajhalf_nhalf*s)
                
                E.append(E2j)
                
                # Fill matrix X
                cont_eq = np.array([A1, B1, C1, D1])
                mom_eq = np.array([A2, B2, C2, D2])    
    
                F[2*xj, 2*xj:2*xj+4] = cont_eq
                F[2*xj+1, 2*xj:2*xj+4] = mom_eq
            
            # append lbc and rbc values
            E.append(lbc_vec[ti])
            E.append(rbc_vec[ti])
            
            # add boundary conditions
            if lbc_type == 'Q':
                F[-2, 0] = 1.0
            else:  # h
                F[-2, 1] = 1.0
                  
            if rbc_type == 'Q':
                F[-1, -2] = 1.0
            else: # h
                F[-1, -1] = 1.0
            
            # solve system
            sol = np.dot(np.linalg.inv(F), np.reshape(np.array(E), [-1, 1]))
            
            #Parse the solutions
            q_iter = sol[::2].reshape(-1)
            h_iter = sol[1::2].reshape(-1)
            
            q_iter_i.append(q_iter)
            h_iter_i.append(h_iter)
            
            Q[ti+1, :] = q_iter
            H[ti+1, :] = h_iter
            
            # correct the bc with the correct values
            if lbc_type == 'Q':
                Q[ti+1, 0] = lbc_vec[ti+1]
            else:  # lbc_type == 'h'
                H[ti+1, 0] = lbc_vec[ti+1]
            
            if rbc_type == 'Q':
                Q[ti+1, -1] = rbc_vec[ti+1]
            else: # h
                H[ti+1, -1] = rbc_vec[ti+1]
            
            
            # Check for stability (large courant number)
            cn = (Q[ti+1, :]/(b*H[ti+1, :]))*dt/dx
            if np.max(np.abs(cn)) > 1.0:
                print('large courant: {0} at step {1}'.format(np.max(np.abs(cn)), ti))
            
            
        # Calculate courant number for all the fields
        area = b * H
        vel = Q/area
        cour = vel * dt/dx
        
    return Q, H, cour
        
#%% Code to animate the results of the objective function
def anim(var):
    fig, ax = plt.subplots()
    plt.title('')
    plt.ylim([np.min(var), np.max(var)])
    line, = ax.plot(var.T[0])
    #sct = ax.scatter(range(var.shape[0]), Q.T[0], c=cm.Blues(var.T[0]))
    
    title = ax.text(0.5, 0.85, "", transform=ax.transAxes, ha="center")
    
    def animate_fig(i):
        title.set_text('Step: {0}'.format(i))
        line.set_ydata(var.T[i])  # update the data
        #sct.se
        return line, title,
    
    ani = FuncAnimation(fig, animate_fig, var.shape[1], interval=100, blit=True)
    plt.show()
    return ani

#%%

# Static test
Q = np.zeros([ts, xs])
H = np.ones([ts, xs])

Q_static, H_static, cour_static = hd_mod(Q, H)
anim(Q_static.T)
anim(H_static.T)
anim(cour_static.T)

#%% Transient test
Q = np.zeros([ts, xs])
H = np.ones([ts, xs])
H[0, :xs/2] = 10.0  # Initial condition is set in the vector 0

Q_trans, H_trans, cour_trans = hd_mod(Q, H)
anim(H_trans.T)




