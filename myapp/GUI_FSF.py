# -*- coding: utf-8 -*-
"""
Created on Mon Mar 05 20:11:33 2018

@author: tah001
"""

import numpy as np
import fsf_model
from bokeh.io import curdoc, show
from bokeh.layouts import widgetbox, gridplot, row, column
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Slider, Button, TextInput, Div, RadioGroup, Select, DataTable
import time


dx = 20
lgth = 1000

DepthIn = 'Depth.inp'
DischargeIn = 'Discharge.inp'
DepthOut = 'WaterDepth.out'
DischargeOut = 'Discharge.out'

hini, qini, distance = fsf_model.readini ('Depth.inp', 'Discharge.inp', dx, lgth)



dsh = ColumnDataSource(data=dict(dist=distance,  h = hini))
dsq = ColumnDataSource(data=dict(dist=distance,  q = qini))

# setup plot
ph = figure(y_range=[0,10], width = 900, height = 300, title="Longitudinal Profile (Water Depth)")
ph.xaxis.axis_label = "Distance (m)"
ph.yaxis.axis_label = "Water Level (m)"
ph.line(x = 'dist', y = 'h', source = dsh, color = 'blue')

pq = figure(y_range=[0,600], width = 900, height = 300, title="Longitudinal Profile (Discharge)")
pq.xaxis.axis_label = "Distance (m)"
pq.yaxis.axis_label = "Discharge (m3/s)"
pq.line(x = 'dist', y = 'q', source = dsq, color = 'red')

# make the widgets
Inhead = Div(text="<b>Input for Free Surface Flow Model</b> <p><i><u>Numerical Parameters:</u></i>")
I_dx = TextInput(value = '500', title = 'Space Interval (m)')
I_dt = TextInput(value = '500', title = 'Time step (sec)')
I_TimeMAX = TextInput(value = '86400', title="Simulation Time (sec)")
I_NMAXIts = TextInput(value = '5', title="Maximum Iteration")
I_theta = TextInput(value = '0.55', title="Theta")
I_Psi = TextInput(value = '0.5', title="Psi")
I_Beta = TextInput(value = '1.0', title="Beta")
head2 = Div(text="<i><u>Physical Parameters:</u></i>")
I_b = TextInput(value = '100', title = 'Channel Width (m)')
I_lgth = TextInput(value = '10000', title = 'Channel Length (m)')
I_Ib = TextInput(value = '0.0001', title = 'Bed Slope')
I_C = TextInput(value = '50', title = 'Chezy Coefficient')

# defining boundary condition
I_ub = Div(text="<b>Upstreame Boundary Condition</b>")
uc_type = RadioGroup(labels=["Discharge", "Water depth"], active=0)
#uc_unit = Select(title="Time unit", value="hours", options=["days", "hours", "min", "sec"])
I_db = Div(text="<br><b>Downstream Boundary Condition</b>")
dc_type = RadioGroup(labels=["Discharge", "Water depth"], active=0)
#dc_unit = Select(title="Time unit", value="hours", options=["days", "hours", "min", "sec"])
I_initial = Div(text="<br><b>Initial Condition</b>")
I_qini = TextInput(value = '0', title = 'Initial Discharge (m3/s)')
I_hini= TextInput(value = '0', title = 'Initial Water Depth (m)')
blank = Div(text="    Tanvir Ahmed")
blank2 = Div(text="<br>")

I_run = Button(label = 'Run Model')
I_animate = Button(label = 'Start Animation')
# define the model


def model_sim ():
    dx = int(I_dx.value)
    dt = int(I_dt.value)
    TimeMAX = int(I_TimeMAX.value)
    NMAXIts = int(I_NMAXIts.value)
    theta = float(I_theta.value)
    Psi = float(I_Psi.value)
    Beta = float(I_Beta.value)
    b = float(I_b.value)
    C = float(I_C.value)
    g = 9.81
    Ib = float(I_Ib.value)
    lgth = int(I_lgth.value)
    UC = 'Q'
    Ufile = 'ubc.txt'
    DC = 'h'
    Dfile = 'dbc.txt'
    ubc, dbc, timenew = fsf_model.readboundary (Ufile, Dfile, dt, TimeMAX)
    hini, qini, distance = fsf_model.readini (DepthIn, DischargeIn, dx, lgth)
    Q, h = fsf_model.fsfCalculation (dx, dt, TimeMAX, NMAXIts, theta, Psi, Beta, b, C, g, Ib, lgth, UC, DC, ubc, dbc, hini, qini)
    def animation ():
        for i in range (len(h)):
            dsh.data=dict(dist=distance,  h = h[i,:])
            dsq.data=dict(dist=distance,  q = Q[i,:])

            time.sleep(0.2)
    # Show the GUI
    I_animate.on_click(animation)

I_run.on_click(model_sim)





#t_wb = widgetbox(l_dx, l_dt)
wb_fsf = widgetbox(Inhead, I_dx, I_dt, I_TimeMAX, I_NMAXIts, I_theta, I_Psi, I_Beta, I_initial, I_qini, I_hini )
wb2_fsf = widgetbox(blank, head2, I_b, I_lgth,I_Ib,I_C, I_ub, uc_type, I_db, dc_type, blank2, I_run )

grid_fsf = gridplot ( [[wb_fsf, wb2_fsf, plot_qsim, ph, pq , I_animate] ,
                   #[tbl_label ],
                   #[data_table ] 
                   ] )

curdoc().add_root(row(wb, wb2, column(ph, pq , I_animate), width = 800))
show(curdoc)    
    